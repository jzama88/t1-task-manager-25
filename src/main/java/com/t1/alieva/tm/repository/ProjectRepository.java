package com.t1.alieva.tm.repository;

import com.t1.alieva.tm.api.repository.IProjectRepository;
import com.t1.alieva.tm.model.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @NotNull
    public Project create(
            @Nullable final String userId,
            @NotNull final String name,
            @NotNull final String description) {
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        return add(project);
    }

    @Override
    @NotNull
    public Project create(
            @Nullable final String userId,
            @NotNull final String name) {
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setUserId(userId);
        return add(project);
    }
}
